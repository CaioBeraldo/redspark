# REDSPARK

- Iniciando ambiente para execução.

```bash
npm install
```

Caso necessário, ajustar a string de conexão `DATABASE_CONNECTION_STRING` com o MongoDB no arquivo .env. 

## Desenvolvimento

- Executando a aplicação em modo de `desenvolvimento`:

```bash
npm run start:dev
```

## Produção

- Executando a aplicação em modo de `produção`:

```bash
npm run start
```

## Testes

- Executando `testes de integração`:

```bash
npm run test
```

## Requisições

- `Criar` uma nova task:

```curl
curl -X POST \
  http://localhost:3000/tasks \
  -d '{
    "name": "Nova tarefa",
    "customer": "Cliente XXX",
    "dueDate": "2019-09-20 18:00",
    "legalDate": "2019-09-20 18:00",
    "fine": true,
    "documents": []
  }'
```

- `Atualizar` uma task existente:

```curl
curl -X PUT \
  http://localhost:3000/tasks/5d882fbc4ea24a3915e14da1 \
  -d '{
    "name": "Nova tarefa",
    "customer": "Cliente XXX",
    "dueDate": "2019-09-20 18:00",
    "legalDate": "2019-09-20 18:00",
    "fine": true,
    "documents": []
  }'
```

- `Remover` uma task:

```curl
curl -X DELETE \
  http://localhost:3000/tasks/5d867ebf8b9e8657581390a3 \
```

- `Adicionar um documento` à uma task existente:

```curl
curl -X POST \
  http://localhost:3000/tasks/documents/5d8831364ea24a3915e14da2 \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F file=@{FILE_PATH}/{FILE_NAME}
```

- `Remover um documento` de uma task:

```curl
curl -X DELETE \
  http://localhost:3000/tasks/documents/5d8831364ea24a3915e14da2 \
  -H 'Content-Type: application/json' \
  -d '{
    "filename": "App.jsx"
  }'
```

- `Lista` todas as tasks:

```curl
curl -X GET \
  http://localhost:3000/tasks \
```
