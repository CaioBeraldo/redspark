'use strict';

import 'reflect-metadata';

import * as dotenv from 'dotenv';

if (process.env.NODE_ENV !== 'production') {
  dotenv.config(); // load configuration from .env file.
}

import { loadAppRoutes } from './routes/index';
import * as Hapi from '@hapi/hapi';
import * as Inert from 'inert';

const initialize = async () => {
  const port = process.env.PORT || 3000;
  const dev = (process.env.NODE_ENV === 'development');

  const server = Hapi.server({
    port,
    host: 'localhost' || '0.0.0.0',
    debug: {
      request: dev ? ['error'] : undefined,
    },
  });

  loadAppRoutes(server);

  await server.register(Inert);

  return server;
};

export const init = async () => {
  const server = await initialize();
  await server.initialize();
  return server;
};

export const start = async () => {
  const server = await initialize();
  await server.start();
  return server;
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

initialize();
