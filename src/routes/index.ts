import taskRoutes from '../tasks/task.controller';

export const loadAppRoutes = async (server) => {
  (await taskRoutes()).forEach((route) => server.route(route));
};
