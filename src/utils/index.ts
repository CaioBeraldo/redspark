import * as Boom from 'boom';
import * as fs from 'fs';

export const extractDataForModel = (model, payload) => {
  const result = {};

  Object.keys(model.schema.obj).forEach((key) => {
    result[key] = payload[key];

    if (!result[key]) {
      throw Boom.badData(`${key} must be defined`);
    }
  });

  return result;
};

const uploadPath = `${__dirname}/upload`;

export const handleFileUpload = ({ file }) => {
  return new Promise((resolve, reject) => {
    const filename = file.hapi.filename;
    const fullpath = `${uploadPath}/${filename}`;
    const data = file._data;

    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath);
    }

    fs.writeFile(fullpath, data, (err) => {
      if (!err) {
        resolve({ filename });
      }

      reject(new Error('Upload failed!'));
    });
  });
};

export const handleFileDeleted = ({ filename }) => {
  return new Promise((resolve, reject) => {
    const fullpath = `${uploadPath}/${filename}`;

    fs.unlink(`${fullpath}`, (err) => {
      if (!err) {
        resolve({ filename });
      }

      reject(new Error('Remove failed!'));
    });
  });
};
