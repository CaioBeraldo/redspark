import Task from './taks.entity';
import { Repository } from '../database/repository';

export class TaskRepository extends Repository {
  createTask = (payload) => {
    const task = new Task(payload);
    return task.save();
  }

  updateTask = (id, payload) => {
    return Task.updateOne({ _id: id }, payload);
  }

  removeTask = async (id) => {
    return await Task.deleteOne({ _id: id });
  }

  getTasks = async () => {
    return await Task.find();
  }

  getTask = async (id) => {
    return await Task.findById(id);
  }

  getTaskStatus({ dueDate, legalDate, fine }): string {
    // A tarefa somente pode ter um status
    const STATUS = {
      OK: 'OK',
      OVERDUE: 'OVERDUE',
      FINE: 'FINE',
    };

    const currentDate = new Date();

    // O status de "com multa" tem precendencia que o de "atrasada"
    // Uma tarefa está com multa quando passa da data legal (`legal_date`) e tem a flag `fine`
    if ((currentDate > legalDate) && (fine)) {
      return STATUS.FINE;
    }

    // Uma tarefa está atrasada quando passa da data meta (`due_date`)
    if (currentDate > dueDate) {
      return STATUS.OVERDUE;
    }

    // Uma tarefa está OK se não tiver atrasada ou gerando multa
    return STATUS.OK;
  }
}
