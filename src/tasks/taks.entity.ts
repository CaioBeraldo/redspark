import * as mongoose from 'mongoose';

const TaskSchema = new mongoose.Schema({
  name: String,
  customer: String,
  dueDate: Date,
  legalDate: Date,
  fine: Boolean,
  documents: Array,
});

export default mongoose.model('Task', TaskSchema);
