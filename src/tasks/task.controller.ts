import { extractDataForModel, handleFileUpload, handleFileDeleted } from '../utils';
import { TaskRepository } from './task.repository';
import Task from './taks.entity';
import * as Boom from 'boom';

class TasksController {
  private taskRepository: TaskRepository;

  constructor() {
    this.taskRepository = new TaskRepository();
  }

  /**
   * Create a new task.
   * @param task {Task} a task object.
   * @type POST /
   */
  createTask = (request, h) => {
    const payload = extractDataForModel(Task, request.payload);
    return this.taskRepository.createTask(payload);
  }

  /**
   * Update a existing task.
   * @param task {Task} a task object.
   * @type PUT /
   */
  updateTask = (request, h) => {
    const payload = extractDataForModel(Task, request.payload);
    return this.taskRepository.updateTask(request.params.id, payload);
  }

  /**
   * Remove a existing task.
   * @param id the task id.
   * @type DELETE /{id}
   */
  removeTask = async (request, h) => {
    return this.taskRepository.removeTask(request.params.id);
  }

  /**
   * Return all tasks.
   * @type GET /
   */
  getTasks = async (request, h) => {
    const tasks = await this.taskRepository.getTasks();
    return tasks.map((task) => ({
      name: task.name,
      customer: task.customer,
      status: this.taskRepository.getTaskStatus(task),
      dueDate: task.dueDate,
      legalDate: task.legalDate,
    }));
  }

  /**
   * Return a task by id.
   * @param id the task id.
   * @type GET /{id}
   */
  getTask = async (request, h) => {
    const task = await this.taskRepository.getTask(request.params.id);

    if (!task) {
      throw Boom.notFound('Task not found');
    }

    return {
      name: task.name,
      customer: task.customer,
      status: this.taskRepository.getTaskStatus(task),
      dueDate: task.dueDate,
      legalDate: task.legalDate,
      fine: task.fine,
      documents: task.documents,
    };
  }

  /**
   * Add a new document to a existing task.
   * @param id the task id.
   * @param file {File} uploaded file stream.
   * @type POST /documents/{id}
   */
  addDocument = async (request, h) => {
    const { payload, params: { id } } = request;

    const task = await this.taskRepository.getTask(id);

    if (!task) {
      throw Boom.notFound('Task not found');
    }

    const result = await handleFileUpload(payload);

    task.documents.push(result);

    return this.taskRepository.updateTask(id, { documents: task.documents});
  }

  /**
   * Remove a document from a existing task.
   * @param id the task id.
   * @param filename the file name to be removed.
   * @type DELETE /documents/{id}
   */
  removeDocument = async (request, h) => {
    const { payload, params: { id } } = request;

    const task = await this.taskRepository.getTask(id);

    if (!task) {
      throw Boom.notFound('Task not found');
    }

    const result = await handleFileDeleted(payload);

    const documentIndex = task.documents.findIndex(d => d.filename === payload.filename);
    task.documents.splice(documentIndex, 1);

    return this.taskRepository.updateTask(id, { documents: task.documents});
  }
}

export default async () => {
  const controller = new TasksController();
  const basePath = '/tasks';

  return [
    {
      method: 'GET',
      path: `${basePath}`,
      handler: controller.getTasks,
    },
    {
      method: 'GET',
      path: `${basePath}/{id}`,
      handler: controller.getTask,
    },
    {
      method: 'POST',
      path: `${basePath}`,
      handler: controller.createTask,
    },
    {
      method: 'PUT',
      path: `${basePath}/{id}`,
      handler: controller.updateTask,
    },
    {
      method: 'DELETE',
      path: `${basePath}/{id}`,
      handler: controller.removeTask,
    },
    {
      method: 'POST',
      path: `${basePath}/documents/{id}`,
      options: {
        payload: {
          output: 'stream',
        },
      },
      handler: controller.addDocument,
    },
    {
      method: 'DELETE',
      path: `${basePath}/documents/{id}`,
      handler: controller.removeDocument,
    },
  ];
};
