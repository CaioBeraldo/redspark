import { getConnection } from '.';

export class Repository {
  private connection;

  constructor() {
    this.init();
  }

  async init() {
    this.connection = await getConnection();
  }
}
