import * as mongoose from 'mongoose';

export const getConnection = async () => {
  const connection = mongoose.connect(process.env.DATABASE_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  return connection;
};
