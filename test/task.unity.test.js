'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { TaskRepository } = require('../dist/tasks/task.repository');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();

describe('Unity tests', () => {
  let taskRepository;

  beforeEach(async () => {
    taskRepository = new TaskRepository();
  });

  describe('Tasks Repository', () => {
    let task;

    it('should create a new task', async () => {
      const name = 'Nova tarefa';

      task = await taskRepository.createTask({
        name,
        customer: 'Cliente XXX',
        dueDate: '2019-09-20 18:00',
        legalDate: '2019-09-20 18:00',
        fine: true,
        documents: [],
      });

      expect(task).to.not.be.empty();
      expect(task.name).to.equals(name);
    });

    it('should update a existing task', async () => {
      const result = await taskRepository.updateTask(task._id, {
        name: 'Tarefa Teste',
      });
      expect(result.nModified).to.equals(1);
    });

    it('should return a existing task', async () => {
      const result = await taskRepository.getTask(task._id);
      expect(result).to.not.be.empty();
    });

    it('should return all existing tasks', async () => {
      const result = await taskRepository.getTasks();
      expect(result).to.be.an.instanceof(Array);
      expect(result.length).to.be.greaterThan(0);
    });

    it('should remove a existing task', async () => {
      const result = await taskRepository.removeTask(task.id);
      expect(result.deletedCount).to.equals(1);
    });
  });
});
