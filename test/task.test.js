'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../dist/server');

describe('Integration tests', () => {
  const invalidId = '6d85c4d4562f1f1af4237f26';
  let validId = '';
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  describe('Tasks endpoint (/tasks)', () => {
    const tasksUrl = '/tasks';

    describe('POST /', () => {
      const method = 'POST';

      it('should create a new task', async () => {
        const res = await server.inject({
          method,
          url: tasksUrl,
          payload: JSON.stringify({
            name: 'Nova tarefa',
            customer: 'Cliente XXX',
            dueDate: '2019-09-20 18:00',
            legalDate: '2019-09-20 18:00',
            fine: true,
            documents: [],
          }),
        });

        validId = res.result.id;

        expect(res.statusCode).to.equal(200);
      });

      it('should not create a new task', async () => {
        const res = await server.inject({
          method,
          url: tasksUrl,
          payload: JSON.stringify({}),
        });

        expect(res.statusCode).to.equal(422);
      });
    });

    describe('GET /', () => {
      const method = 'GET';

      it('should return a task', async () => {
        const res = await server.inject({ method, url: `${tasksUrl}/${validId}` });
        expect(res.statusCode).to.equal(200);
      });

      it('should return a error', async () => {
        const { result } = await server.inject({ method, url: `${tasksUrl}/${invalidId}` });
        expect(result.statusCode).to.equal(404);
      });

      it('should return all tasks', async () => {
        const res = await server.inject({ method, url: tasksUrl });
        expect(res.statusCode).to.equal(200);
      });
    });

    describe('PUT /', () => {
      const method = 'PUT';

      it('should update a task', async () => {
        const res = await server.inject({
          method,
          url: `${tasksUrl}/${validId}`,
          payload: JSON.stringify({
            name: 'Nova tarefa 2',
            customer: 'Cliente YYY',
            dueDate: '2019-09-20 18:00',
            legalDate: '2019-09-20 18:00',
            fine: true,
            documents: [],
          }),
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.nModified).to.equal(1);
      });

      it('should not update a task', async () => {
        const res = await server.inject({
          method,
          url: `${tasksUrl}/${invalidId}`,
          payload: JSON.stringify({
            name: 'Nova tarefa 2',
            customer: 'Cliente YYY',
            dueDate: '2019-09-20 18:00',
            legalDate: '2019-09-20 18:00',
            fine: true,
            documents: [],
          }),
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.nModified).to.equal(0);
      });
    });

    describe('Documents endpoint (/tasks/documents)', () => {
      const documentsUrl = '/tasks/documents';

      describe('POST /', () => {
        const method = 'POST';

        it('should add a new document to a existing task', async () => {
          const multipartPayload =
            '--AaB03x\r\n' +
            'content-disposition: form-data; name="file"; filename="file1.txt"\r\n' +
            'Content-Type: text/plain\r\n' +
            '\r\n' +
            '... contents of file1.txt ...\r\r\n' +
            '--AaB03x--\r\n';

          const res = await server.inject({
            method,
            url: `${documentsUrl}/${validId}`,
            payload: multipartPayload,
            headers: {
              'content-type': 'multipart/form-data; boundary=AaB03x',
            },
          });

          expect(res.statusCode).to.equal(200);
        });
      });

      describe('DELETE /', () => {
        const method = 'DELETE';

        it('should remove a document from a existing task', async () => {
          const res = await server.inject({
            method,
            url: `${documentsUrl}/${validId}`,
            payload: JSON.stringify({
              filename: 'file1.txt',
            }),
          });

          expect(res.statusCode).to.equal(200);
        });
      });
    });

    describe('DELETE /', () => {
      const method = 'DELETE';

      it('should remove a task', async () => {
        const res = await server.inject({ url: `${tasksUrl}/${validId}`, method });
        expect(res.statusCode).to.equal(200);
        expect(res.result.deletedCount).to.equal(1);
      });

      it('should not exist', async () => {
        const { statusCode } = await server.inject({ url: `${tasksUrl}/${validId}`, method: 'GET' });
        expect(statusCode).to.equal(404);
      });

      it('should not remove a task', async () => {
        const res = await server.inject({ url: `${tasksUrl}/${validId}`, method });
        expect(res.statusCode).to.equal(200);
        expect(res.result.deletedCount).to.equal(0);
      });
    });
  });
});
